import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class FilterContent extends StatelessWidget {
  final String icon;
  final String label;
  final bool choosed;
  final VoidCallback? function;

  const FilterContent({
    Key? key,
    required this.icon,
    required this.label,
    required this.choosed,
    this.function,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: ListTile(
        onTap: function,
        leading: SvgPicture.asset(
          icon,
        ),
        title: Text(label),
        trailing: choosed
            ? const Icon(
                Icons.check,
                color: InMeColors.black,
              )
            : null,
      ),
    );
  }
}
