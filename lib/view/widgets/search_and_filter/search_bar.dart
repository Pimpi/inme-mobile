import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class SearchBar extends StatelessWidget {
  final void Function(String) onChanged;

  const SearchBar({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      elevation: 3,
      child: TextField(
        onChanged: onChanged,
        style: Theme.of(context).textTheme.caption,
        decoration: InputDecoration(
          hintText: 'Type here...',
          contentPadding: const EdgeInsets.only(
            top: 15,
            right: 16,
          ),
          hintStyle: Theme.of(context)
              .textTheme
              .caption!
              .copyWith(color: InMeColors.grey),
          prefixIcon: const Icon(Icons.search, color: InMeColors.grey),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
