import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/animations.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:lottie/lottie.dart';

class InMeLoadingAnimation extends StatelessWidget {
  const InMeLoadingAnimation({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      key: const Key('loading_animation'),
      child: Column(
        children: [
          SizedBox(height: Sizes.screenHeight(context) * 0.25),
          Lottie.asset(
            InMeAnimations.climbStairs,
            height: Sizes.screenWidth(context) * 0.7,
            width: Sizes.screenWidth(context) * 0.7,
          ),
          const SizedBox(height: 20.0),
          AnimatedTextKit(
            repeatForever: true,
            animatedTexts: [
              TyperAnimatedText(
                Words.sendingYourRequest,
                textStyle: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: InMeColors.green),
              ),
              TyperAnimatedText(
                Words.pleaseWait,
                textStyle: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: InMeColors.green),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
