import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class LongButton extends StatelessWidget {
  const LongButton({
    Key? key,
    required this.label,
    required this.onTap,
    required this.color,
  }) : super(key: key);

  final String label;
  final Function() onTap;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(color),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: color)))),
      onPressed: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              label,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2!
                  .copyWith(color: InMeColors.white),
            ),
            const Icon(Icons.arrow_forward_ios, color: InMeColors.white),
          ],
        ),
      ),
    );
  }
}
