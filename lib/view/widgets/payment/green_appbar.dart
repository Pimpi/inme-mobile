import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class GreenAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;
  final String title;
  final bool arrowBack;

  const GreenAppBar({
    Key? key,
    required this.appBar,
    required this.title,
    required this.arrowBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: arrowBack,
      elevation: 0,
      backgroundColor: InMeColors.green,
      title: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .headline4!
            .copyWith(color: InMeColors.white),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
