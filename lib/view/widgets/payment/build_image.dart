import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:inme_mobile/utils/sizes.dart';

Widget buildImage(BuildContext context, String imagePath) => ConstrainedBox(
      child: SvgPicture.asset(imagePath),
      constraints: BoxConstraints(
        maxWidth: Sizes.screenWidth(context) * 0.8,
        maxHeight: Sizes.screenWidth(context) * 1.0,
      ),
    );
