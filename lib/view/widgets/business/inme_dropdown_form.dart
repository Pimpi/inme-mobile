import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class DropdownForm extends StatelessWidget {
  const DropdownForm({
    Key? key,
    required List<String> categoryList,
    required this.onCategorySelected,
    this.validator,
    this.defaultValue,
  })  : _categoryList = categoryList,
        super(key: key);

  final List<String> _categoryList;
  final String? Function(String?)? validator;
  final void Function(String?)? onCategorySelected;
  final String? defaultValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      elevation: 2,
      validator: validator,
      value: defaultValue,
      // hint: defaultValue != null ? Text(defaultValue!) : null,
      items: _categoryList
          .map((categoryLabel) => DropdownMenuItem(
                value: categoryLabel,
                child: Text(categoryLabel),
              ))
          .toList(),
      style: Theme.of(context).textTheme.bodyText2,
      dropdownColor: InMeColors.white,
      decoration: InputDecoration(
        isDense: true,
        contentPadding: const EdgeInsets.symmetric(
          vertical: 6,
          horizontal: 10,
        ),
        fillColor: InMeColors.white,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide.none,
        ),
      ),
      onChanged: onCategorySelected,
    );
  }
}
