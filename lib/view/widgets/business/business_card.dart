import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import '../../../utils/routes/routes.dart';

class BusinessCard extends StatelessWidget {
  const BusinessCard({
    Key? key,
    required this.business,
  }) : super(key: key);

  final Business business;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Material(
            borderRadius: BorderRadius.circular(20),
            elevation: 4,
            child: ListTile(
              title: Text(business.name),
              leading: CircleAvatar(
                backgroundImage: NetworkImage(business.pictures),
                radius: 30.0,
              ),
              subtitle: Text(
                business.description,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              onTap: () {
                Navigator.pushNamed(context, Routes.viewOwnBusiness,
                    arguments: business);
              },
            )));
  }
}
