import 'package:flutter/material.dart';
import 'package:inme_mobile/models/investor.dart';

import '../../../utils/api/api_constants.dart';

class ListInvestorCard extends StatelessWidget {
  const ListInvestorCard({
    Key? key,
    required this.investor,
  }) : super(key: key);

  final Investor investor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Material(
        borderRadius: BorderRadius.circular(20),
        elevation: 4,
        child: ListTile(
            dense: true,
            key: const Key('company card'),
            onTap: () {},
            contentPadding:
                const EdgeInsets.symmetric(vertical: 7.0, horizontal: 16.0),
            leading: CircleAvatar(
              backgroundImage: NetworkImage("$baseUrl${investor.picture}"),
              radius: 30.0,
            ),
            title: Text(
              investor.name as String,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
            ),
            subtitle: paddingInvestor(investor, context)),
      ),
    );
  }
}

Widget paddingInvestor(Investor investor, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: [
        const Text("Rp."),
        Expanded(
          child: Text(
            investor.amount.toString(),
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      ],
    ),
  );
}
