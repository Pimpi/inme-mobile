import 'dart:io';
import 'package:image_picker/image_picker.dart';

import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class ImageField extends StatelessWidget {
  const ImageField({
    Key? key,
    required XFile? xFileImage,
  })  : _xFileImage = xFileImage,
        super(key: key);

  final XFile? _xFileImage;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        color: InMeColors.white,
        width: Sizes.screenWidth(context),
        height: Sizes.screenWidth(context) * 0.5,
        child: _xFileImage != null
            ? Image.file(
                File(_xFileImage!.path),
                fit: BoxFit.fill,
              )
            : const Icon(
                Icons.image_outlined,
                color: InMeColors.grey,
                size: 50,
              ),
      ),
    );
  }
}
