import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/api_constants.dart';

class CompanyCardBusiness extends StatelessWidget {
  const CompanyCardBusiness({
    Key? key,
    required this.business,
    required this.checker,
  }) : super(key: key);

  final BusinessHistory business;
  final bool checker;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Material(
        borderRadius: BorderRadius.circular(20),
        elevation: 4,
        child: ListTile(
          dense: true,
          key: const Key('company card'),
          onTap: () {},
          contentPadding:
              const EdgeInsets.symmetric(vertical: 7.0, horizontal: 16.0),
          leading: CircleAvatar(
            backgroundImage: NetworkImage("$baseUrl${business.picture}"),
            radius: 30.0,
          ),
          title: Text(
            business.name as String,
            style: Theme.of(context).textTheme.headline6!.copyWith(
                  fontWeight: FontWeight.w700,
                ),
          ),
          subtitle: checker == true
              ? paddingInvestmenHistory(business, context)
              : null,
        ),
      ),
    );
  }
}

Widget paddingInvestmenHistory(BusinessHistory business, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: [
        Text(
          "-Rp.",
          style: TextStyle(color: Colors.redAccent.shade700),
        ),
        Expanded(
          child: Text(
            business.investedAmount.toString(),
            style: Theme.of(context)
                .textTheme
                .bodyText2!
                .copyWith(color: Colors.redAccent.shade700),
          ),
        ),
      ],
    ),
  );
}
