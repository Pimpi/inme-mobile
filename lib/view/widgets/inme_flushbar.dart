import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class InMeFlushBar {
  static showSuccess(BuildContext context, String text) {
    Flushbar(
      backgroundColor: InMeColors.green,
      borderRadius: BorderRadius.circular(15),
      boxShadows: [
        BoxShadow(
          offset: const Offset(2, 4),
          blurRadius: 4,
          color: Colors.black.withOpacity(0.25),
        ),
      ],
      margin: const EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 8.0,
      ),
      shouldIconPulse: false,
      icon: const Icon(
        Icons.check,
        color: Colors.white,
      ),
      messageText: Text(text,
          style: Theme.of(context)
              .textTheme
              .button!
              .copyWith(color: Colors.white)),
      duration: const Duration(seconds: 3),
    ).show(context);
  }

  static showFailed(BuildContext context, String text) {
    Flushbar(
      backgroundColor: InMeColors.red,
      borderRadius: BorderRadius.circular(15),
      boxShadows: [
        BoxShadow(
          offset: const Offset(2, 4),
          blurRadius: 4,
          color: Colors.black.withOpacity(0.25),
        ),
      ],
      margin: const EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 8.0,
      ),
      shouldIconPulse: false,
      icon: const Icon(
        Icons.report_outlined,
        color: Colors.white,
      ),
      messageText: Text(text,
          style: Theme.of(context)
              .textTheme
              .button!
              .copyWith(color: Colors.white)),
      duration: const Duration(seconds: 3),
    ).show(context);
  }
}
