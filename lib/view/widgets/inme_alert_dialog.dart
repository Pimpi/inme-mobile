import 'package:flutter/material.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';

class InMeAlertDialog extends StatelessWidget {
  const InMeAlertDialog({
    Key? key,
    required this.title,
    required this.buttonText1,
    required this.buttonFunction1,
    required this.buttonColor1,
    this.buttonText2,
    this.buttonFunction2,
    this.buttonColor2,
  }) : super(key: key);
  final String title;
  final String buttonText1;
  final VoidCallback buttonFunction1;
  final Color buttonColor1;
  final String? buttonText2;
  final VoidCallback? buttonFunction2;
  final Color? buttonColor2;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      title: Text(
        title,
        textAlign: TextAlign.center,
      ),
      actionsAlignment: MainAxisAlignment.spaceEvenly,
      actions: [
        InMeRoundedButton(
          backgroundColor: buttonColor1,
          onPressed: buttonFunction1,
          text: buttonText1,
        ),
        if (buttonText2 != null &&
            buttonFunction2 != null &&
            buttonColor2 != null)
          InMeRoundedButton(
            backgroundColor: buttonColor2!,
            onPressed: buttonFunction2!,
            text: buttonText2!,
          ),
      ],
    );
  }
}
