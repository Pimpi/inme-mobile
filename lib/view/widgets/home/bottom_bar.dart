import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    Key? key,
    this.current = 0,
  }) : super(key: key);

  final int current;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
      child: SizedBox(
        height: Sizes.screenHeight(context) * 0.1,
        child: BottomNavigationBar(
          currentIndex: current,
          iconSize: Sizes.screenHeight(context) * 0.04,
          selectedItemColor: InMeColors.yellow,
          unselectedItemColor: InMeColors.white,
          backgroundColor: InMeColors.green,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              label: Words.home,
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
              ),
              label: Words.search,
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.chat,
              ),
              label: Words.chat,
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                key: Key(
                  'settings',
                ),
              ),
              label: Words.settings,
            ),
          ],
        ),
      ),
    );
  }
}
