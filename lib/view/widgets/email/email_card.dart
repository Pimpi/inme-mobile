import 'package:flutter/material.dart';
import 'package:inme_mobile/models/message.dart';
import 'package:inme_mobile/providers/_providers.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EmailCard extends StatelessWidget {
  const EmailCard({
    Key? key,
    required this.message,
  }) : super(key: key);

  final Message message;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      elevation: 4,
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        onTap: () {
          Navigator.pushNamed(context, Routes.emailDetail, arguments: message);
        },
        leading: CircleAvatar(
          backgroundImage: NetworkImage(
            message.sender['profile']['picture'],
          ),
          radius: 20,
        ),
        title: Consumer<UserProvider>(
          builder: (context, user, _) => Text(
            message.sender['email'] == user.user.email
                ? 'Sent to: ${message.receiver['profile']['nama'].toString()}'
                : 'From: ${message.sender['profile']['nama'].toString()}',
            style: Theme.of(context).textTheme.headline6!.copyWith(
                  fontWeight: FontWeight.w700,
                  color: message.sender['email'] == user.user.email
                      ? InMeColors.logoGreen
                      : InMeColors.logoBlue,
                ),
          ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            message.text,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              DateFormat('d/M').format(message.date),
            ),
            Text(
              DateFormat('HH:mm').format(message.date),
            ),
          ],
        ),
      ),
    );
  }
}
