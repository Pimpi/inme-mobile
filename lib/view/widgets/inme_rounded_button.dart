import 'package:flutter/material.dart';

class InMeRoundedButton extends StatelessWidget {
  const InMeRoundedButton({
    Key? key,
    required this.backgroundColor,
    required this.onPressed,
    required this.text,
  }) : super(key: key);
  final String text;
  final Color backgroundColor;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(text),
      style: TextButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        backgroundColor: backgroundColor,
        padding: EdgeInsets.zero,
        primary: Colors.white,
        textStyle: Theme.of(context).textTheme.button,
      ),
      onPressed: onPressed,
    );
  }
}