import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';

class ReceivedChatBubble extends StatelessWidget {
  const ReceivedChatBubble({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: EdgeInsets.only(
            bottom: Sizes.screenHeight(context) * 0.01,
          ),
          child: Material(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.zero,
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            elevation: 4,
            color: InMeColors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: Sizes.screenWidth(context) * 0.03,
                vertical: Sizes.screenHeight(context) * 0.01,
              ),
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: Sizes.screenWidth(context) * 0.45,
                ),
                child: Text(
                  'Wah anjay',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
