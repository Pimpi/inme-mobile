import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';

class MessageCard extends StatelessWidget {
  const MessageCard({
    Key? key,
    required this.account,
  }) : super(key: key);

  final String account;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      elevation: 4,
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        onTap: () {
          Navigator.pushNamed(context, Routes.messageDetail);
        },
        onLongPress: () {
          showDialog(
            context: context,
            builder: (context) => InMeAlertDialog(
              title: Words.deleteAlert,
              buttonText1: Words.cancel,
              buttonColor1: InMeColors.red,
              buttonFunction1: () => Navigator.of(context).pop(),
              buttonText2: Words.delete,
              buttonColor2: InMeColors.logoBlue,
              buttonFunction2: () {},
            ),
          );
        },
        leading: const CircleAvatar(
          backgroundImage: AssetImage(
            InMeImages.inMeLogo,
          ),
          radius: 20,
        ),
        title: Text(
          account,
          style: Theme.of(context).textTheme.headline6!.copyWith(
                fontWeight: FontWeight.w700,
              ),
        ),
        subtitle: const Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Text(
            'Isi chatnya',
            maxLines: 2,
          ),
        ),
      ),
    );
  }
}
