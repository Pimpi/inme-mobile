import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/user_api.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/business/company_card_business.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';

class InvestedBusiness extends StatelessWidget {
  const InvestedBusiness({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return InvestedBusines(listBusiness: UserApi.getInvestmenHistory());
  }
}

class InvestedBusines extends StatelessWidget {
  final Future<List<BusinessHistory>>? listBusiness;
  const InvestedBusines({Key? key, this.listBusiness}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: GreenAppBar(
          appBar: AppBar(),
          title: Words.investedBusiness,
          arrowBack: false,
        ),
        body: FutureBuilder(
          future: listBusiness,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.done:
                var listData = snapshot.data;
                if (listData.length == 0) {
                  return const Center(child: Text(Words.noInvestedBusiness));
                }
                return ListView.builder(
                    itemCount: listData.length,
                    itemBuilder: (BuildContext context, int index) {
                      BusinessHistory business = listData[index];

                      return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: CompanyCardBusiness(business: business, checker: false,));
                    });
              default:
                return const Center(child: CircularProgressIndicator(color: InMeColors.green,));
            }
          },
        ));
  }
}
