import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/models/investor.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/business/list_investor_card.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';

class InvestorList extends StatelessWidget {
  const InvestorList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final businessDetail = ModalRoute.of(context)!.settings.arguments as Business;
    return InvestorLists(listInvestor: BusinessApi.getInvestorList(businessDetail.id));
  }
}

class InvestorLists extends StatelessWidget {
  final Future<List<Investor>>? listInvestor;
  const InvestorLists({Key? key, this.listInvestor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GreenAppBar(
          appBar: AppBar(),
          title: Words.investorList,
          arrowBack: false,
        ),
        body: FutureBuilder(
          future: listInvestor,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.done:
                var listData = snapshot.data;
                if (listData.length == 0) {
                  return const Center(child: Text(Words.noInvestor));
                }
                return ListView.builder(
                    itemCount: listData.length,
                    itemBuilder: (BuildContext context, int index) {
                      Investor investor = listData[index];
                      return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: ListInvestorCard(investor: investor));
                    });
              default:
                return const Center(child: CircularProgressIndicator(color: InMeColors.green));
            }
          },
        ));
  }
}
