import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/view/widgets/message/widgets.dart';

class MessageDetail extends StatelessWidget {
  const MessageDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: InMeColors.green,
        child: SafeArea(
          child: Container(
            color: InMeColors.moreWhite,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  child: Container(
                    color: InMeColors.green,
                    height: Sizes.screenHeight(context) * 0.125,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Sizes.screenWidth(context) * 0.05,
                        vertical: Sizes.screenHeight(context) * 0.03,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const CircleAvatar(
                            backgroundImage: AssetImage(
                              InMeImages.inMeLogo,
                            ),
                            radius: 30,
                          ),
                          SizedBox(
                            width: Sizes.screenWidth(context) * 0.02,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Hasna Nadifah',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(color: InMeColors.white),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  bottom: 5,
                                ),
                                child: Text(
                                  'Investor from UI Inc',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2!
                                      .copyWith(color: InMeColors.white),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.only(
                      left: Sizes.screenWidth(context) * 0.05,
                      right: Sizes.screenWidth(context) * 0.05,
                      top: Sizes.screenHeight(context) * 0.03,
                      bottom: Sizes.screenHeight(context) * 0.01,
                    ),
                    children: const [
                      ReceivedChatBubble(),
                      SentChatBubble(),
                      SentChatBubble(),
                      ReceivedChatBubble(),
                      ReceivedChatBubble(),
                      ReceivedChatBubble(),
                      SentChatBubble(),
                      SentChatBubble(),
                      ReceivedChatBubble(),
                      SentChatBubble(),
                      SentChatBubble(),
                      SentChatBubble(),
                      ReceivedChatBubble(),
                      ReceivedChatBubble(),
                      ReceivedChatBubble(),
                      SentChatBubble(),
                      SentChatBubble(),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Sizes.screenWidth(context) * 0.04,
                    vertical: Sizes.screenHeight(context) * 0.01,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Material(
                          color: InMeColors.grey,
                          borderRadius: BorderRadius.circular(15),
                          child: TextField(
                            onChanged: (value) {},
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 16,
                              ),
                              hintText: 'Type your message here...',
                              border: InputBorder.none,
                              hintStyle: Theme.of(context).textTheme.bodyText2,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: Sizes.screenWidth(context) * 0.03,
                      ),
                      const Icon(
                        Icons.send,
                        color: InMeColors.logoGreen,
                        size: 30,
                      ),
                      SizedBox(
                        width: Sizes.screenWidth(context) * 0.03,
                      ),
                      const Icon(
                        Icons.camera_alt,
                        color: InMeColors.logoBlue,
                        size: 30,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
