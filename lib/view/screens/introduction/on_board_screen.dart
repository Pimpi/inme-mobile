import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:provider/provider.dart';

class OnBoardScreen extends StatefulWidget {
  const OnBoardScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardScreen> createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  bool _isLoading = true;
  @override
  void initState() {
    super.initState();
    _tryAutoLogin();
  }

  void _tryAutoLogin() async {
    var response =
        await Provider.of<UserProvider>(context, listen: false).autoLogin();
    if (response == ResponseStatus.success) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(Routes.home, (route) => false);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Scaffold(
            body: Center(
              child: CircularProgressIndicator(color: InMeColors.green),
            ),
          )
        : IntroductionScreen(
            showNextButton: true,
            showSkipButton: true,
            pages: [
              PageViewModel(
                title: Words.inMe,
                body: Words.onBoardingFirstPage,
                image: _buildImage(context, InMeIcons.inMeLogo),
                decoration: _buildDecoration(context),
              ),
              PageViewModel(
                title: Words.payment,
                body: Words.onBoardingSecondPage,
                image: _buildImage(context, InMeIcons.onBoarding2),
                decoration: _buildDecoration(context),
              ),
              PageViewModel(
                title: Words.manage,
                body: Words.onBoardingThirdPage,
                image: _buildImage(context, InMeIcons.onBoarding3),
                decoration: _buildDecoration(context),
              ),
            ],
            skip: _buildText(context, Words.skip, InMeColors.green),
            next: _buildText(context, Words.next, InMeColors.green),
            done: _buildText(context, Words.getStarted, InMeColors.moreWhite),
            doneStyle: TextButton.styleFrom(backgroundColor: InMeColors.green),
            onDone: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, Routes.signIn, (Route<dynamic> route) => false);
            },
          );
  }

  PageDecoration _buildDecoration(BuildContext context) => PageDecoration(
        titleTextStyle: Theme.of(context).textTheme.headline1!,
        bodyTextStyle: Theme.of(context).textTheme.subtitle1!,
      );

  Widget _buildImage(BuildContext context, String imagePath) => ConstrainedBox(
        child: SvgPicture.asset(imagePath),
        constraints: BoxConstraints(
          maxWidth: Sizes.screenWidth(context) * 0.6,
          maxHeight: Sizes.screenWidth(context) * 0.6,
        ),
      );

  Text _buildText(BuildContext context, String text, Color color) => Text(
        text,
        style: Theme.of(context).textTheme.button!.copyWith(color: color),
      );
}
