import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/api/register_api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/sizes.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _isLoading = false;
  final _registerNameFormKey = GlobalKey<FormState>();

  final _formController = {
    "email": TextEditingController(),
    "password": TextEditingController(),
  };

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Scaffold(body: InMeLoadingAnimation())
        : Scaffold(
            body: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Register',
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .headline2!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                          ),
                          child: _buildImage(context, InMeIcons.inMeLogo),
                        ),
                        Form(
                          key: _registerNameFormKey,
                          child: _createForm(context),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Column _createForm(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InMeFormField(
          labelText: Words.email,
          type: TextInputType.text,
          placeHolder: Words.email,
          validatorFunction: _isFormNull,
          controller: _formController["email"],
        ),
        InMeFormField(
          obscureText: true,
          labelText: Words.password,
          type: TextInputType.text,
          placeHolder: Words.password,
          validatorFunction: _isFormNull,
          controller: _formController["password"],
        ),
        const SizedBox(height: 10.0),
        _buildButtons(context),
      ],
    );
  }

  String? _isFormNull(String? value) {
    if (value == null || value.isEmpty) {
      return Words.cannotEmpty;
    }
    return null;
  }

  Row _buildButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: InMeRoundedButton(
              text: Words.registerButton,
              backgroundColor: InMeColors.green,
              onPressed: () {
                if (_registerNameFormKey.currentState!.validate()) {
                  final data = {
                    "email": _formController["email"]!.text,
                    "password": _formController["password"]!.text,
                  };
                  _submit(data);
                }
              }),
        ),
      ],
    );
  }

  void _submit(Map<String, dynamic> data) async {
    setState(() {
      _isLoading = true;
    });

    final response = await RegisterApi.register(data);
    if (response == ResponseStatus.success) {
      Navigator.pushNamed(context, Routes.signIn);
      InMeFlushBar.showSuccess(context, 'Register success');
    } else {
      setState(() {
        _isLoading = false;
      });
      InMeFlushBar.showFailed(context, Words.savingDataFailed);
    }
  }
}

Widget _buildImage(BuildContext context, String imagePath) => ConstrainedBox(
      child: SvgPicture.asset(imagePath),
      constraints: BoxConstraints(
        maxWidth: Sizes.screenWidth(context) * 0.6,
        maxHeight: Sizes.screenWidth(context) * 0.6,
      ),
    );
