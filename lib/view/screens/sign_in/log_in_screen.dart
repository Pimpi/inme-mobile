import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<Login> {
  final _loginNameFormKey = GlobalKey<FormState>();
  final bool _isLoading = false;

  final _formController = {
    "email": TextEditingController(),
    "password": TextEditingController(),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? const InMeLoadingAnimation()
          : CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Login',
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .headline2!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                          ),
                          child: _buildImage(context, InMeIcons.inMeLogo),
                        ),
                        Form(
                          key: _loginNameFormKey,
                          child: _createForm(context),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Column _createForm(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InMeFormField(
          labelText: Words.email,
          type: TextInputType.emailAddress,
          placeHolder: Words.email,
          validatorFunction: _isFormNull,
          controller: _formController["email"],
        ),
        InMeFormField(
          obscureText: true,
          labelText: "password",
          type: TextInputType.text,
          placeHolder: "password",
          validatorFunction: _isFormNull,
          controller: _formController["password"],
        ),
        const SizedBox(height: 10.0),
        _buildButtons(context),
        Center(
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, Routes.register);
            },
            child: const Text(
              'Sign Up',
            ),
          ),
        ),
      ],
    );
  }

  String? _isFormNull(String? value) {
    if (value == null || value.isEmpty) {
      return Words.cannotEmpty;
    }
    return null;
  }

  Row _buildButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: InMeRoundedButton(
            text: "Login",
            backgroundColor: InMeColors.green,
            onPressed: login,
          ),
        ),
      ],
    );
  }

  void login() async {
    // setState(() {
    //   _isLoading = true;
    // });
    final response = await SignInApi.login({
      "email": _formController["email"]!.text,
      "password": _formController["password"]!.text
    });

    if (response == ResponseStatus.success) {
      await Provider.of<UserProvider>(context, listen: false).setUser();
      Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
      InMeFlushBar.showSuccess(context, 'Sign in successful.');
    } else {
      // setState(() {
      //   _isLoading = false;
      // });
      const snackBar = SnackBar(content: Text("Sign in failed."));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}

Widget _buildImage(BuildContext context, String imagePath) => ConstrainedBox(
      child: SvgPicture.asset(imagePath),
      constraints: BoxConstraints(
        maxWidth: Sizes.screenWidth(context) * 0.6,
        maxHeight: Sizes.screenWidth(context) * 0.6,
      ),
    );
