import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/help_center/guideline_card_widget.dart';

class BusinessGuideline extends StatelessWidget {
  const BusinessGuideline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _businessGuideline = [
      {
        'title': '1. Can’t involve prohibited items',
        'description':
        'Business that are illegal, heavily regulated, or potentially dangerous for backers, as well as rewards that the creator did not make.',
      },
      {
        'title': '2. Prohibited using misleading imagery',
        'description':
        'Includes photorealistic renderings and heavily edited or manipulated images or videos that could give backers a false impression of a product’s current stage of development.',
      },
      {
        'title': '3. Must have prototype demonstration',
        'description':
        'Should reflect a product’s current state and should not include any CGI or special effects to demonstrate functionality that does not yet exist.',
      },
      {
        'title': '4. Prohibited SARA',
        'description':
        'Description of the product is prohibited containing SARA.',
      },
    ];

    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: Text(
          Words.businessGuideline,
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: InMeColors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView.builder(
          itemCount: _businessGuideline.length,
          itemBuilder: (context, index) {
            return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child:
                GuidelineCard(guideline: _businessGuideline, index: index));
          }),
    );
  }
}
