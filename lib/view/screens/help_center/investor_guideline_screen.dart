import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/help_center/guideline_card_widget.dart';

class InvestorGuideline extends StatelessWidget {
  const InvestorGuideline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _investorGuideline = [
      {
        'title': '1. Investor Company Minimum Profit',
        'description':
            'Investor must have profit at least 1 billion rupiah for each month to minimize the inability of investors when injecting money.',
      },
      {
        'title': '2. Background Assessment',
        'description':
            'Before investor can invest, InMe team will do background check first, if it is deemed possible, then you can invest.',
      },
      {
        'title': '3. Investor Information',
        'description':
            'All Investor Information provided by the Investor during the registration process is accurate, such Investor Information may be used by the Company to comply with all applicable legal requirements.',
      },
      {
        'title': '4. Never invest in anything you don’t understand',
        'description':
            'Before you put your money into any investment, take time to research it thoroughly, so you understand exactly what’s involved and what the risks are.',
      },
      {
        'title':
            '5. The bigger the potential returns, the higher the level of risk',
        'description':
            'Think carefully about your approach to risk. Remember though, that no investment comes without risk, and there is always the chance you could get back less than you put in.',
      }
    ];

    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: Text(
          Words.investorGuideline,
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: InMeColors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView.builder(
          itemCount: _investorGuideline.length,
          itemBuilder: (context, index) {
            return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child:
                    GuidelineCard(guideline: _investorGuideline, index: index));
          }),
    );
  }
}
