import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/view/widgets/help_center/guideline_card_widget.dart';

class MessagesGuideline extends StatelessWidget {
  const MessagesGuideline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _messagesGuideline = [
      {
        'title': '1. Be clear',
        'description':
        'Give detail information about your business so there will be no miscommunication.',
      },
    ];

    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: Text(
          'Messages Guideline',
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: InMeColors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView.builder(
          itemCount: _messagesGuideline.length,
          itemBuilder: (context, index) {
            return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child:
                GuidelineCard(guideline: _messagesGuideline, index: index));
          }),
    );
  }
}
