import 'package:flutter/material.dart';
import 'package:inme_mobile/view/screens/payment/payment_information_screen.dart';
import 'package:inme_mobile/view/widgets/payment/build_payment_information.dart';

class PaymentSuccess extends StatelessWidget {
  const PaymentSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final paymentArguments =
        ModalRoute.of(context)?.settings.arguments as PaymentArguments;
    return BuildPaymentInformation(
      success: true,
      paymentArguments: paymentArguments,
    );
  }
}
