import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';
import 'payment_confirmation_screen.dart';

class PaymentLoading extends StatefulWidget {
  const PaymentLoading({Key? key}) : super(key: key);

  @override
  _PaymentLoadingState createState() => _PaymentLoadingState();
}

class _PaymentLoadingState extends State<PaymentLoading> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: GreenAppBar(
        appBar: AppBar(),
        title: Words.paymentConfirmation,
        arrowBack: false,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: buildPaymentConfirmation(
                context,
                InMeIcons.paymentConfirmationLoading,
                Words.paymentLoading,
                Words.paymentLoadingSub),
          ),
        ],
      ),
    );
  }
}
