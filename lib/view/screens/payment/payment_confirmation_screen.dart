import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/api/api_payment.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/screens/payment/payment_information_screen.dart';
import 'package:inme_mobile/view/widgets/inme_alert_dialog.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';
import 'package:inme_mobile/view/widgets/payment/build_image.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';
import 'package:intl/intl.dart';

import '../../widgets/inme_flushbar.dart';

class PaymentConfirmation extends StatefulWidget {
  const PaymentConfirmation({Key? key}) : super(key: key);

  @override
  _PaymentConfirmationState createState() => _PaymentConfirmationState();
}

class _PaymentConfirmationState extends State<PaymentConfirmation> {
  late DateTime deadlinePayment;
  late DateTime now;

  @override
  void initState() {
    super.initState();
    setState(() {
      deadlinePayment = DateTime.now().add(const Duration(hours: 1));
      now = DateTime.now();
    });
  }

  @override
  Widget build(BuildContext context) {
    final String deadlineDate = DateFormat('d/M/y').format(deadlinePayment);
    final String deadlineHour = DateFormat.Hm().format(deadlinePayment);

    final paymentArguments =
        ModalRoute.of(context)?.settings.arguments as PaymentArguments;
    final paymentResponse = paymentArguments.paymentResponse;

    String virtualAccount = paymentResponse.vaNumbers?[0]["va_number"];
    // String bankName = paymentResponse.vaNumbers?[0]["bank"];

    DateTime deadline = paymentResponse.expiredTime as DateTime;

    Future<bool> onWillPop() async {
      final shouldPop = await showDialog(
          context: context,
          builder: (context) => InMeAlertDialog(
                title: Words.alertCancelPayment,
                buttonText1: Words.no,
                buttonFunction1: () => Navigator.of(context).pop(false),
                buttonColor1: InMeColors.red,
                buttonText2: Words.yes,
                buttonColor2: InMeColors.logoBlue,
                buttonFunction2: () => Navigator.popUntil(context,
                    (route) => route.settings.name == Routes.viewBusiness),
              ));
      return shouldPop ?? false;
    }

    return WillPopScope(
      onWillPop: () => onWillPop(),
      child: Scaffold(
        backgroundColor: InMeColors.moreWhite,
        appBar: GreenAppBar(
          appBar: AppBar(),
          title: Words.paymentConfirmation,
          arrowBack: false,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(50.0, 50.0, 50.0, 10.0),
              child: buildPaymentConfirmation(
                  context,
                  InMeIcons.paymentConfirmationLoading,
                  Words.waitingPayment,
                  Words.waitingPaymentSub),
            ),
            Text(deadlineDate),
            Text(deadlineHour),
            const SizedBox(
              height: 25.0,
            ),
            Text(
              "Virtual Account Code :",
              style: Theme.of(context).textTheme.subtitle1?.copyWith(
                  color: InMeColors.black, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 25,
            ),
            Text(
              virtualAccount,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2
                  ?.copyWith(color: InMeColors.black),
            ),
            const SizedBox(
              height: 50.0,
            ),
            SizedBox(
              width: Sizes.screenWidth(context) * 0.4,
              child: InMeRoundedButton(
                  backgroundColor: InMeColors.logoBlue,
                  onPressed: () {
                    if (!now.isBefore(deadline)) {
                      Navigator.popUntil(
                          context,
                          (route) =>
                              route.settings.name == Routes.paymentInformation);
                    } else {
                      _handleCheckStatus(paymentArguments, context);
                    }
                  },
                  text: Words.paidThebill),
            )
          ],
        ),
      ),
    );
  }
}

// Navigator.pushNamed(context, Routes.paymentSuccess,
// arguments: paymentResponse);

Widget buildPaymentConfirmation(
    BuildContext context, String image, String headline1, String headline2) {
  return Column(
    children: [
      const SizedBox(
        height: 20.0,
      ),
      Center(child: buildImage(context, image)),
      const SizedBox(
        height: 12.0,
      ),
      Text(
        headline1,
        style: Theme.of(context)
            .textTheme
            .subtitle1
            ?.copyWith(color: InMeColors.black, fontWeight: FontWeight.bold),
      ),
      const SizedBox(
        height: 7.0,
      ),
      Text(
        headline2,
        style: Theme.of(context)
            .textTheme
            .subtitle2
            ?.copyWith(color: InMeColors.black),
      ),
    ],
  );
}

void _handleCheckStatus(
    PaymentArguments paymentArguments, BuildContext context) async {
  final paymentResponse = paymentArguments.paymentResponse;
  final response = await PaymentAPI.checkStatus(paymentResponse.id);

  paymentArguments.paymentResponse = response!;

  if (response.status == "pending") {
    InMeFlushBar.showFailed(context, Words.pendingPayment);
  } else if (response.status == "failure") {
    Navigator.pushNamed(context, Routes.paymentUnsuccess,
        arguments: paymentArguments);
  } else {
    Navigator.pushNamed(context, Routes.paymentSuccess,
        arguments: paymentArguments);
  }
}
