import 'package:flutter/material.dart';
import 'package:inme_mobile/models/message.dart';
import 'package:inme_mobile/utils/api/message_api.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/white_appbar.dart';
import 'package:inme_mobile/view/widgets/email/widgets.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class EmailListScreen extends StatelessWidget {
  const EmailListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future<List<Message>> listMessage = MessageApi.getMyMessages();
    return Scaffold(
      appBar: WhiteAppBar(
        appBar: AppBar(),
        title: Words.message,
      ),
      body: FutureBuilder(
        future: listMessage,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              var listMessage = snapshot.data;
              if (listMessage.length == 0) {
                return const Center(
                  child: Text(
                    Words.noMessageFound,
                  ),
                );
              }
              return ListView.builder(
                itemCount: listMessage.length,
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                      bottom: 10,
                    ),
                    child: EmailCard(
                      message: listMessage[index],
                    ),
                  );
                },
              );
            default:
              return const Center(
                child: CircularProgressIndicator(
                  color: InMeColors.green,
                ),
              );
          }
        },
      ),
    );
  }
}
