import 'package:flutter/material.dart';
import 'package:inme_mobile/models/message.dart';
import 'package:inme_mobile/providers/_providers.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EmailDetailScreen extends StatelessWidget {
  const EmailDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final messageDetail = ModalRoute.of(context)!.settings.arguments as Message;
    return Scaffold(
      body: Container(
        color: InMeColors.green,
        child: SafeArea(
          child: Container(
            color: InMeColors.moreWhite,
            child: Consumer<UserProvider>(
              builder: (context, user, _) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    child: Container(
                      color: InMeColors.green,
                      height: Sizes.screenHeight(context) * 0.125,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Sizes.screenWidth(context) * 0.05,
                          vertical: Sizes.screenHeight(context) * 0.03,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const CircleAvatar(
                              backgroundImage: AssetImage(
                                InMeImages.inMeLogo,
                              ),
                              radius: 30,
                            ),
                            SizedBox(
                              width: Sizes.screenWidth(context) * 0.02,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  messageDetail.sender['email'] ==
                                          user.user.email
                                      ? 'Sent to: ${messageDetail.receiver['profile']['nama'].toString()}'
                                      : 'From: ${messageDetail.sender['profile']['nama'].toString()}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2!
                                      .copyWith(color: InMeColors.white),
                                ),
                                Text(
                                  DateFormat('HH:mm d/M')
                                      .format(messageDetail.date),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2!
                                      .copyWith(color: InMeColors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      padding: EdgeInsets.only(
                        left: Sizes.screenWidth(context) * 0.05,
                        right: Sizes.screenWidth(context) * 0.05,
                        top: Sizes.screenHeight(context) * 0.03,
                        bottom: Sizes.screenHeight(context) * 0.01,
                      ),
                      children: [
                        Text(
                          messageDetail.text,
                        ),
                        if (messageDetail.sender['email'] != user.user.email)
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 20,
                            ),
                            child: InMeRoundedButton(
                              backgroundColor: InMeColors.logoBlue,
                              onPressed: () {
                                Navigator.pushNamed(
                                  context,
                                  Routes.emailCreate,
                                  arguments: {
                                    'id': messageDetail.sender['id'],
                                    'name': messageDetail.sender['profile']
                                            ['nama']
                                        .toString(),
                                  },
                                );
                              },
                              text: 'Reply',
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
