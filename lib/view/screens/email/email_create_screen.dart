import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/_providers.dart';
import 'package:inme_mobile/utils/api/message_api.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:provider/provider.dart';

import '../../../utils/routes/routes.dart';

class EmailCreateScreen extends StatefulWidget {
  const EmailCreateScreen({Key? key}) : super(key: key);

  @override
  State<EmailCreateScreen> createState() => _EmailCreateScreenState();
}

class _EmailCreateScreenState extends State<EmailCreateScreen> {
  final bool _isLoading = false;
  final _emailFormKey = GlobalKey<FormState>();
  final textInput = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final opponentData = ModalRoute.of(context)!.settings.arguments as Map;
    return _isLoading
        ? const Scaffold(body: InMeLoadingAnimation())
        : Scaffold(
            appBar: WhiteAppBar(
              appBar: AppBar(),
              title: Words.createMessage,
            ),
            body: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Send to: ${opponentData['name']}',
                        ),
                        Form(
                          key: _emailFormKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 16.0),
                              InMeFormField(
                                labelText: Words.text,
                                type: TextInputType.multiline,
                                isExpanded: true,
                                validatorFunction: _isFormNull,
                                controller: textInput,
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: InMeRoundedButton(
                                      text: Words.cancel,
                                      backgroundColor: InMeColors.red,
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ),
                                  const SizedBox(width: 20),
                                  Expanded(
                                    child: Consumer<UserProvider>(
                                      builder: (context, user, _) => InMeRoundedButton(
                                        text: Words.send,
                                        backgroundColor: InMeColors.logoBlue,
                                        onPressed: () {
                                          final data = {
                                            "sender": user.user.email,
                                            "receiver": opponentData['id'],
                                            "message": textInput.text,
                                          };
                                          MessageApi.createMessage(data);
                                          // Navigator.popUntil(context, Ro);
                                          Navigator.pushNamedAndRemoveUntil(
                                              context, Routes.home, (route) => false);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  String? _isFormNull(String? value) {
    if (value == null || value.isEmpty) {
      return Words.cannotEmpty;
    }
    return null;
  }
}
