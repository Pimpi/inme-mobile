import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:inme_mobile/utils/api/api_constants.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';

class RegisterApi {
  static Future<ResponseStatus> register(Map<String, dynamic> data) async {
    const String path = '$baseUrl/api/v1/register/';
    final response = await http.post(
      Uri.parse(path),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encoder.convert(data),
    );

    if (response.statusCode == 201) {
      return ResponseStatus.success;
    } else {
      return ResponseStatus.error;
    }
  }
}
