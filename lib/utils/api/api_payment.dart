import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/utils/api/api.dart';

class PaymentAPI {
  static Future<PaymentResponse?> chargePayment(
      Map<String, dynamic> data) async {
    final response =
        await apiHelperInstance.postWithAuthHeader("payment/charge/", data);

    if (response["status"] == 200) {
      return PaymentResponse.fromJson(response["data"]);
    } else {
      return null;
    }
  }

  static Future<PaymentResponse?> checkStatus(String? orderId) async {
    final response =
        await apiHelperInstance.getWithAuthHeader("payment/status/$orderId");

    if (response["status"] == 200) {
      return PaymentResponse.fromJson(response["data"]);
    } else {
      return null;
    }
  }

  static Future<List<Map<String, dynamic>>?> getPaymentHistory() async {
    try {
      final response = await apiHelperInstance.getWithAuthHeader("payment/");
      if (response["status"] == 200) {
        final decodedResponse = (response["data"] as List)
            .map((e) => e as Map<String, dynamic>)
            .toList();
        return decodedResponse;
      }
      return null;
    } catch (e) {
      return null;
    }
  }
}
