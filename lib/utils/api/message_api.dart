import 'package:inme_mobile/models/message.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';

class MessageApi {
  static Future<List<Message>> getMyMessages() async {
    Map<String, dynamic> response =
        await apiHelperInstance.getWithAuthHeader("email/current/");

    List<dynamic> collection = response["data"];
    List<Message> _listMessage =
        collection.map((json) => Message.fromJson(json)).toList();
    _listMessage.sort((b, a) => a.date.compareTo(b.date));

    return _listMessage;
  }

  static Future<ResponseStatus> createMessage(Map<String, dynamic> data) async {
    final response = await apiHelperInstance.postWithAuthHeader("email/", data);

    if (response["status"] == 201) {
      return ResponseStatus.success;
    } else if (response["data"].containsKey("non_field_errors")) {
      return ResponseStatus.duplicate;
    } else {
      return ResponseStatus.error;
    }
  }
}
