import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import 'package:inme_mobile/utils/constants/response_status.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'api_constants.dart';

class SignInApi {
  static Future<ResponseStatus> login(Map<String, dynamic> data) async {
    const String path = '$baseUrl/api/v1/token/';
    final response = await http.post(
      Uri.parse(path),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encoder.convert(data),
    );

    if (response.statusCode == 200) {
      final decodedBody = json.decoder.convert(response.body);
      final prefs = await SharedPreferences.getInstance();
      log('access token: ' + decodedBody['access']);
      await prefs.setString('refresh', decodedBody['refresh']);
      await prefs.setString('access', decodedBody['access']);

      return ResponseStatus.success;
      // final decodedBody = json.decoder.convert(response.body);
      // log(decodedBody['access']);
      // Map<String, dynamic> decodedToken = JwtDecoder.decode(decodedBody['access']);
      // log(decodedToken.toString());
    } else {
      return ResponseStatus.error;
    }
  }
}

Future<Map<String, dynamic>> loginToAPI() async {
  // var idToken =
  //     (await googleSignInApiInstance.getCurrentUser()?.authentication)?.idToken;
  // var accessToken =
  //     (await googleSignInApiInstance.getCurrentUser()?.authentication)
  //         ?.accessToken;

  // var res = await apiHelperInstance.post(
  //     '/oauth/resource', {'id_token': idToken, 'access_token': accessToken});
  // if (res["status"] == 400) {
  //   // retry
  //   var user = await googleSignInApiInstance.refresh();
  //   idToken = (await user?.authentication)?.idToken;
  //   accessToken = (await user?.authentication)?.accessToken;
  //   res = await apiHelperInstance.post(
  //       '/oauth/resource', {'id_token': idToken, 'access_token': accessToken});
  //   log("retry success");
  // }
  return {};

  // return res;
}
