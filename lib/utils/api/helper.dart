import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:inme_mobile/utils/api/api_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiHelper {
  final String _baseUrl;
  final http.Client _client;

  ApiHelper({http.Client? client, String? baseUrl})
      : _client = client ?? http.Client(),
        _baseUrl = baseUrl ?? "http://localhost:8080/api/v1";

  Future<Map<String, dynamic>> post(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final url = '$_baseUrl$path';
    final response = await _client.post(Uri.parse(url),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    final decodedBody = json.decode(response.body) as Map<String, dynamic>;
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> put(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final response = await _client.put(Uri.parse('$_baseUrl$path'),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    final decodedBody = json.decode(response.body) as Map<String, dynamic>;
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> get(String path,
      [Map<String, String>? headers]) async {
    final response = await _client.get(
      Uri.parse('$_baseUrl$path'),
      headers: headers,
    );
    log(response.body);
    final decodedBody = json.decode(response.body);
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> delete(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final url = '$_baseUrl$path';

    final response = await _client.delete(Uri.parse(url),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    return {"status": response.statusCode};
  }

  Future<Map<String, dynamic>> getWithAuthHeader(String path) async {
    final prefs = await SharedPreferences.getInstance();

    var idToken = prefs.getString('access');

    var res = await get(path, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var response = await post(
        'token/refresh/',
        {
          "refresh": prefs.getString('refresh'),
        },
      );
      await prefs.setString('access', response['data']['access']);
      idToken = prefs.getString('access');

      res = await get(path, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> postWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    final prefs = await SharedPreferences.getInstance();

    var idToken = prefs.getString('access');

    var res = await post(path, body, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var response = await post(
        'token/refresh/',
        {
          "refresh": prefs.getString('refresh'),
        },
      );
      await prefs.setString('access', response['data']['access']);
      idToken = prefs.getString('access');

      res = await post(path, body, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> putWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    final prefs = await SharedPreferences.getInstance();

    var idToken = prefs.getString('access');

    var res = await put(path, body, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var response = await post(
        'token/refresh/',
        {
          "refresh": prefs.getString('refresh'),
        },
      );
      await prefs.setString('access', response['data']['access']);
      idToken = prefs.getString('access');

      res = await put(path, body, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> deleteWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    final prefs = await SharedPreferences.getInstance();

    var idToken = prefs.getString('access');

    var res = await delete(path, {}, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var response = await post(
        'token/refresh/',
        {
          "refresh": prefs.getString('refresh'),
        },
      );
      await prefs.setString('access', response['data']['access']);
      idToken = prefs.getString('access');

      res = await delete(path, {}, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<int> createBusiness(String path, Map<String, dynamic> body) async {
    final url = '$_baseUrl$path';
    // final request = http.MultipartRequest("POST", Uri.parse(url));
    final prefs = await SharedPreferences.getInstance();
    final idToken = prefs.getString('access');
    var request = await _createBusinessRequest(
      url,
      body,
      idToken!,
    );

    var response = await request.send();
    if (response.statusCode > 399) {
      final newToken = await tokenRetry();
      request = await _createBusinessRequest(url, body, newToken);

      response = await request.send();
      log(response.statusCode.toString());
    }
    log(response.statusCode.toString());
    return response.statusCode;
  }

  Future<http.MultipartRequest> _createBusinessRequest(
      String url, Map<String, dynamic> body, String token) async {
    final request = http.MultipartRequest("POST", Uri.parse(url));

    request.headers['authorization'] = 'Bearer $token';
    request.fields['business_name'] = body['business_name'];
    request.fields['category'] = body['category'];
    request.fields['description'] = body['description'];
    request.fields['no_npwp'] = body['no_npwp'];
    request.fields['nama_bank'] = body['nama_bank'];
    request.fields['no_rekening'] = body['no_rekening'];
    request.fields['target'] = body['target'].toString();
    request.files.add(
      await http.MultipartFile.fromPath('pictures', body['pictures']),
    );
    return request;
  }

  Future<int> updateBusiness(Map<String, dynamic> body) async {
    final url = '${_baseUrl}business/${body["id"]}/';
    final prefs = await SharedPreferences.getInstance();
    final idToken = prefs.getString('access');
    var request = await _updateBusinessRequest(
      url,
      body,
      idToken!,
    );
    var response = await request.send();
    if (response.statusCode > 399) {
      final newToken = await tokenRetry();
      request = await _updateBusinessRequest(url, body, newToken);

      response = await request.send();
      log(response.statusCode.toString());
    }
    log(response.statusCode.toString());
    return response.statusCode;
  }

  Future<http.MultipartRequest> _updateBusinessRequest(
      String url, Map<String, dynamic> body, String token) async {
    final request = http.MultipartRequest("PUT", Uri.parse(url));

    request.headers['authorization'] = 'Bearer $token';
    request.fields['business_name'] = body['business_name'];
    request.fields['category'] = body['category'];
    request.fields['description'] = body['description'];
    request.fields['no_npwp'] = body['no_npwp'];
    request.fields['nama_bank'] = body['nama_bank'];
    request.fields['no_rekening'] = body['no_rekening'];
    request.fields['target'] = body['target'].toString();
    if (body['picture'] != null) {
      request.files.add(
        await http.MultipartFile.fromPath('pictures', body['pictures']),
      );
    }
    return request;
  }

  Future<void> updateProfile(String path, Map<String, dynamic> body) async {
    final url = '$_baseUrl$path';
    final prefs = await SharedPreferences.getInstance();
    final idToken = prefs.getString('access');
    var request = await _updateProfileRequest(url, body, idToken!);

    var response = await request.send();
    if (response.statusCode > 399) {
      final newToken = await tokenRetry();
      request = await _updateProfileRequest(url, body, newToken);

      response = await request.send();
      log(response.statusCode.toString());
    }
    log(response.statusCode.toString());
  }

  Future<http.MultipartRequest> _updateProfileRequest(
      String url, Map<String, dynamic> body, String token) async {
    final request = http.MultipartRequest("PUT", Uri.parse(url));

    request.headers['authorization'] = 'Bearer $token';
    request.fields['nama'] = body['nama'];
    request.fields['no_telp'] = body['no_telp'];
    request.fields['no_ktp'] = body['no_ktp'];
    request.fields['no_npwp'] = body['no_npwp'];
    if (body['picture'] != null) {
      request.files.add(
        await http.MultipartFile.fromPath('picture', body['picture']),
      );
    }
    return request;
  }

  Future<String> tokenRetry() async {
    final prefs = await SharedPreferences.getInstance();

    var response = await post(
      'token/refresh/',
      {
        "refresh": prefs.getString('refresh'),
      },
    );
    await prefs.setString('access', response['data']['access']);
    return prefs.getString('access')!;
  }
}

var apiHelperInstance = ApiHelper(baseUrl: '$baseUrl/api/v1/');
