import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/api.dart';

class UserApi {
  static Future<Map<String, dynamic>> getUser() async {
    final response =
        await apiHelperInstance.getWithAuthHeader("accounts/resource/");

    if (response["status"] == 200) {
      return response["data"];
    } else {
      return {"status": "error"};
    }
  }

  static Future<void> updateUser(
      Map<String, dynamic> data) async {
    
        await apiHelperInstance.updateProfile("accounts/resource/edit/", data);
    // if (response["status"] == 200) {
    //   return response["data"]["profile"];
    // } else {
    //   return {"status": "error"};
    // }
  }

  static Future<List<BusinessHistory>> getInvestmenHistory() async {
    Map<String, dynamic> response = await apiHelperInstance
        .getWithAuthHeader("investment/myinvestment/");

    List<dynamic> collection = response["data"];
    List<BusinessHistory> _listBusiness =
    collection.map((json) => BusinessHistory.fromJson(json)).toList();

    return _listBusiness;
  }
}
