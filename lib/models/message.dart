class Message {
  Map<String, dynamic> sender;
  Map<String, dynamic> receiver;
  String text;
  DateTime date;

  Message({
    required this.sender,
    required this.receiver,
    required this.text,
    required this.date,
  });

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      sender: json['sender'],
      receiver: json['receiver'],
      text: json['message'],
      date: DateTime.parse(json['created']).add(
        const Duration(
          hours: 7,
        ),
      ),
    );
  }
}
