class PaymentInfo {
  String? businessName;
  double? investAmount;
  double platformFee = 50000;
  double? totalPayment;
  String? paymentOption;
  String? codePayment;
  int? businessId;

  PaymentInfo(
      {required this.businessName,
      required this.investAmount,
      required this.totalPayment,
      required this.paymentOption,
      required this.codePayment,
        required this.businessId});
}

class PaymentResponse {
  String? id;
  int? investorId;
  int? investedBusinessId;
  double? amount;
  List<dynamic>? vaNumbers;
  String? status;
  DateTime? expiredTime;

  PaymentResponse({
    required this.id,
    required this.investorId,
    required this.investedBusinessId,
    required this.amount,
    required this.vaNumbers,
    required this.status,
    required this.expiredTime,

});

  factory PaymentResponse.fromJson(Map<String, dynamic> json) {
    return PaymentResponse(
      id: json['payment_id'],
      investorId: json['investor_id'],
      investedBusinessId: json['invested_business_id'],
      amount: double.parse(json['amount']),
      vaNumbers: json["va_numbers"],
      expiredTime: DateTime.parse(json["expired_time"]),
      status: json['status'],
    );
  }

}
