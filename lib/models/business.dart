class Business {
  int id;
  String name;
  String description;
  String category;
  double target;
  double investedFunds;
  String pictures;
  String npwp;
  String? bankName;
  String? noRekening;
  int userId;

  Business({
    required this.id,
    required this.name,
    required this.description,
    required this.target,
    required this.category,
    required this.investedFunds,
    required this.pictures,
    required this.npwp,
    required this.bankName,
    required this.noRekening,
    required this.userId,
  });

  factory Business.fromJson(Map<String, dynamic> json) {
    return Business(
      id: json['id'],
      name: json['business_name'],
      description: json['description'],
      category: json['category'],
      target: double.parse(json["target"]),
      investedFunds: double.parse(json["invested_funds"]),
      npwp: json['no_npwp'],
      noRekening: json['no_rekening'],
      bankName: json['nama_bank'],
      pictures: '${json["pictures"][0]["picture"]}',
      userId: json['user_id'],
    );
  }

  @override
  bool operator ==(Object other) => other is Business && other.name == name;

  @override
  int get hashCode => name.hashCode;
}

class BusinessHistory {
  String? name;
  double? investedAmount;
  String? picture;

  BusinessHistory({required this.name, required this.picture, required this.investedAmount});

  factory BusinessHistory.fromJson(Map<String, dynamic> json) {
    return BusinessHistory(
      picture: json['picture'],
      name: json['business_name'],
      investedAmount: double.parse(json['amount']),
    );
  }
}
