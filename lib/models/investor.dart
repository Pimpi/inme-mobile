class Investor {
  String? picture;
  String? name;
  double amount;
  String? bankOption;

  Investor(
      {required this.picture,
        required this.name,
        required this.amount,
       });

  factory Investor.fromJson(Map<String, dynamic> json) {
    return Investor(
        picture: json['picture'],
        name: json['name'],
        amount: double.parse(json['amount']) ,
    );
  }
}
