import 'package:inme_mobile/utils/api/api_constants.dart';

class User {
  User({
    required this.name,
    required this.email,
    this.phoneNumber,
    this.nik,
    this.npwp,
    this.picture
  });

  String name;
  String email;
  String? phoneNumber;
  String? nik;
  String? npwp;
  String? picture;

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"] ?? json["email"][0],
        email: json["email"],
        phoneNumber: json["no_telp"],
        nik: json["no_ktp"],
        npwp: json["no_npwp"],
        picture: '$baseUrl${json["picture"]}'
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "phoneNumber": phoneNumber,
        "nik": nik,
        "npwp": npwp,
      };
}
