import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/utils/routes/routes_factory.dart';
import 'package:inme_mobile/view/screens/settings/settings_screen.dart';
import 'package:inme_mobile/view/screens/screens.dart';


void main() {
  testWidgets('Test the help center button is tapped', (WidgetTester tester) async {
    final helpcenter = find.byKey(
      const Key(
        'help center',
      ),
    );

    await tester.pumpWidget(MaterialApp(
      home: const Settings(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));

    await tester.tap(helpcenter); // Animate to the last page of on board
    await tester.pumpAndSettle();

    expect(find.text('Help Center'), findsOneWidget); // Make sure that the current screen is home page
  });

  // testWidgets('Test the profile button is tapped', (WidgetTester tester) async {
  //   final profile = find.byKey(
  //     const Key(
  //       'profile',
  //     ),
  //   );

  //   await tester.pumpWidget(MaterialApp(
  //     home: const Settings(),
  //     onGenerateRoute: (settings) {
  //       return MaterialPageRoute(
  //         builder: (_) => getScreenByName(settings.name!),
  //         settings: settings,
  //       );
  //     },
  //   ));

  //   await tester.tap(profile); // Animate to the last page of on board
  //   await tester.pumpAndSettle();

  //   expect(find.text('Profile'), findsOneWidget); // Make sure that the current screen is home page
  // });
}
