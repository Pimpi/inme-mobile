import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/help_center/faq.dart';



void main() {
  testWidgets('Test the FAQ widget is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: Faq()));
    expect(find.text('Frequently Asked Questions'), findsOneWidget);
    expect(find.text('1. What is InMe?'), findsOneWidget);
    expect(find.text('2. What are the fees?'), findsOneWidget);
    expect(find.text('3. How to edit my profile?'), findsOneWidget);
    expect(find.text('4. What if i can’t pay by the payment deadline?'), findsOneWidget);
  });

}
