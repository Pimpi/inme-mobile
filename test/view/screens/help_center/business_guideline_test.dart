import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/help_center/business_guideline.dart';



void main() {
  testWidgets('Test the BusinessGuideline widget is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: BusinessGuideline()));
    expect(find.text('Business Guideline'), findsOneWidget);
    expect(find.text('1. Can’t involve prohibited items'), findsOneWidget);
    expect(find.text('2. Prohibited using misleading imagery'), findsOneWidget);
    expect(find.text('3. Must have prototype demonstration'), findsOneWidget);
    expect(find.text('4. Prohibited SARA'), findsOneWidget);
  });
}
