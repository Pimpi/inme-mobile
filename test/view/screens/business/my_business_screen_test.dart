import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the my business is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: MyBusiness()));
    expect(find.text('My Business'), findsOneWidget);
  });
}
