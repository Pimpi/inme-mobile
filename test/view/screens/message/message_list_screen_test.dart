import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the message list is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: MessageList()));
    expect(find.text('Message'), findsOneWidget);
  });
}
