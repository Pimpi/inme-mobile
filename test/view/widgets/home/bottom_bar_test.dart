import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/home/bottom_bar.dart';

void main() {
  testWidgets('Test the BottomBar widget is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: BottomBar()));
    expect(find.text('Home'), findsOneWidget);
    expect(find.text('Search'), findsOneWidget);
    expect(find.text('Chat'), findsOneWidget);
    expect(find.text('Settings'), findsOneWidget);
  });
}
