import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/success_screen.dart';

void main() {
  testWidgets("Test successScreen", (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SuccessScreen(text: "Sucess")));

    expect(find.text("Back to homepage"), findsWidgets);
  });
}
