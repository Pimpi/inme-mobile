import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/search_and_filter/search_bar.dart';

void main() {
  testWidgets("Test SearchBar is pumped", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: SearchBar(
          key: _key,
          onChanged: (_) {},
        ),
      ),
    ));

    expect(find.byKey(_key), findsWidgets);
  });
}
