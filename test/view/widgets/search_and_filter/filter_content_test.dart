import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/view/widgets/search_and_filter/filter_content.dart';

void main() {
  testWidgets("Test FilterContent is pumped", (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: FilterContent(
          label: 'Label',
          choosed: false,
          icon: InMeIcons.foodFilter,
        ),
      ),
    ));

    expect(find.text('Label'), findsWidgets);
  });
  testWidgets("Test FilterContent with key", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: FilterContent(
          key: _key,
          label: 'Label',
          choosed: false,
          icon: InMeIcons.foodFilter,
        ),
      ),
    ));

    expect(find.text('Label'), findsWidgets);
  });
}
