import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/loadiang_animation.dart';

void main() {
  testWidgets("Test loading animation", (WidgetTester tester) async {
    await tester.pumpWidget(
      const MaterialApp(
        home: Scaffold(
          body: SingleChildScrollView(child: InMeLoadingAnimation()),
        ),
      ),
    );

    expect(find.byKey(const Key('loading_animation')), findsWidgets);
  });
  testWidgets("Test loading animation with key", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SingleChildScrollView(child: InMeLoadingAnimation(key: _key)),
        ),
      ),
    );

    expect(find.byKey(const Key('loading_animation')), findsWidgets);
  });
}
