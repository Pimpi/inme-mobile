import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/inme_flushbar.dart';

void main() {
  testWidgets("Test Flushbar success", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: GestureDetector(
        key: _key,
        onTap: () {
          InMeFlushBar.showSuccess(_key.currentContext!, 'success');
        },
        child: const Text('text'),
      ),
    ));

    await tester.tap(find.byKey(_key));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    await tester.pumpAndSettle(const Duration(seconds: 5));
    expect(find.text('text'), findsOneWidget);
  });
  testWidgets("Test Flushbar failed", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: GestureDetector(
        key: _key,
        onTap: () {
          InMeFlushBar.showFailed(_key.currentContext!, 'failed');
        },
        child: const Text('text'),
      ),
    ));

    await tester.tap(find.byKey(_key));
    await tester.pumpAndSettle(const Duration(seconds: 1));
    await tester.pumpAndSettle(const Duration(seconds: 5));
    expect(find.text('text'), findsOneWidget);
  });
}
