import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/business/inme_dropdown_form.dart';

void main() {
  testWidgets("Inme dropdown is pumped", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: DropdownForm(
          key: _key,
          categoryList: const [
            'food',
            'pet',
          ],
          onCategorySelected: (_) {},
        ),
      ),
    ));

    expect(find.byKey(_key), findsOneWidget);
  });
}
