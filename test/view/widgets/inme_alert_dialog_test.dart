import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/inme_alert_dialog.dart';

void main() {
  testWidgets("Test alert dialog is pumped", (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: InMeAlertDialog(
          title: 'Title',
          buttonText1: 'text1',
          buttonFunction1: () {},
          buttonColor1: Colors.white,
        ),
      ),
    ));

    expect(find.text('Title'), findsOneWidget);
  });
  testWidgets("Test alert dialog and button 2 is not null", (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: InMeAlertDialog(
          title: 'Title',
          buttonText1: 'text1',
          buttonFunction1: () {},
          buttonColor1: Colors.white,
          buttonText2: 'text2',
          buttonFunction2: () {},
          buttonColor2: Colors.white,
        ),
      ),
    ));

    expect(find.text('Title'), findsOneWidget);
  });
}
