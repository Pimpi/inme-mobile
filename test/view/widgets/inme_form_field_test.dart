import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/inme_form_field.dart';

void main() {
  testWidgets("Test Formfield", (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                  hasScrollBody: false,
                  child: Column(
                    children: const [
                      InMeFormField(labelText: "text"),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
    expect(find.text('text'), findsOneWidget);
  });
  testWidgets("Test formfield with key", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                  hasScrollBody: false,
                  child: Column(
                    children: [
                      InMeFormField(key: _key, labelText: "text"),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
    expect(find.text('text'), findsOneWidget);
  });
}
