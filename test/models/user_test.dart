import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/user.dart';

void main() {
  test("Test user model constructor", () {
    User data = User(
      name: "User 1",
      email: "user1@gmail.com",
    );

    expect(data.name, "User 1");
  });

  test("Test user fromJson function", () {
    final data = {"name": "User 1", "email": "user1@gmail.com"};

    User user = User.fromJson(data);

    expect(user.name, "User 1");
  });

  test("Test user toJson function", () {
    User data = User(
      name: "User 1",
      email: "user1@gmail.com",
    );

    final map = data.toJson();

    expect(map["name"], "User 1");
  });
}
