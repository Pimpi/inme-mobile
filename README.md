[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-mobile/badges/development/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-mobile/-/commits/development)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-mobile/badges/development/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-mobile/-/commits/development)
#### InMe (Invest in Me)
where investors meet entrepreneurs.

#### Git rules
1. Do *flutter analyze* before push the code into gitlab
2. Allowed commit message prefixes: fix, update, implement, move, add, delete, create, rename
3. Commit message format: [RED/GREEN/REFACTOR] commit-message
4. Create new branch for every task, merge request to development when done
5. Merge request rules:
- set assignee to yourself
- set reviewer to syabib, adamy, & rasyid
- squash commit
- *DON'T delete source branch*
